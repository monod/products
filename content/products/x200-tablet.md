{
    "title": "Lenovo ThinkPad X200 Tablet",
    "date": "2019-09-03T22:35:06+05:30",
    "tags": ["portatil", "libreboot", "coreboot", "thinkpad", "x200"],
    "categories": ["Portatil", "Tablet"],
    "images": ["img/x200-tablet/frente.png", "img/x200-tablet/lateral1.png", "img/x200-tablet/lateral2.png", "img/x200-tablet/penPoint.png"],
    "thumbnailImage": "img/x200-tablet/frente.png",
    "actualPrice": "$460.000",
    "comparePrice": null,
    "inStock": true,
    "options": {
            "RAM": ["2GB"],
			"Procesador":["Intel ® Core ™ Duo CPU  @ 1.60GHz"],
			"CPU": ["2"],
			"Base": ["no"],
			"Bios": ["Coreboot/SeaBios", "Librebot"],
            "Disco Duro" : ["460GB"],
			"Bateria": ["47%"],
			"S.O": ["Gnu/Linux"],
			"Distribución": ["Parabola", "PureOS", "Trisquel 8"]
    }
}

Tablet de segunda mano x200 de la línea ThinkPad. Permite girar la pantalla para su uso en forma de tablet. La pantalla funciona con un dispositivo apuntador (penTouch) el cuál está incluido.
